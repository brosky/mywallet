# -*- coding: utf-8 -*-

from odoo import models, fields, api

class saldo(models.Model):
    _name = 'mywallet.transaksi'
    name = fields.Char("Keterangan")
    jenis_transaksi = fields.Char()
    kategori = fields.Many2one("mywallet.kategori")
    tanggal = fields.Date("Tanggal", required=True)
    nominal = fields.Float()
    akun_asal = fields.Many2one("mywallet.saldo",)
    akun_tujuan = fields.Many2one("mywallet.saldo")

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100