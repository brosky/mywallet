# -*- coding: utf-8 -*-
{
    'name': "My Wallet",

    'summary': """
        Aplikasi untuk mencatat pemasukan dan pengeluaran harian.
        """,

    'description': """
        Aplikasi untuk mencatat pemasukan dan pengeluaran harian.
    """,

    'author': "Brosky Samson Putra Halim, S.Kom",
    'website': "https://www.sonlim.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/10.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Expenses',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/saldo.xml',
        'views/transaksi.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}